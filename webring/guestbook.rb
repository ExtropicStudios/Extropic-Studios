#!/usr/bin/env ruby

require 'cgi'
require 'rubygems'
require 'sqlite3'

begin

  # open the sqlite db, creating it if it doesn't exist
  db = SQLite3::Database.open "db/guestbook.db"
  if (!db) then
    db = SQLite3::Database.new "#{Core::DBPATH}"
    db.execute "CREATE TABLE IF NOT EXISTS guestbook(
      guestbook_id INTEGER PRIMARY KEY,
      message TEXT,
      name STRING,
      date DATETIME)"
  end

  # get all the guestbook crap.
  # TODO: might need to paginate this at some point in the future
  results = []
  db.execute "SELECT message, name, date FROM guestbook g ORDER BY date ASC" do |row|
    results << {'message' => row[0], 'name' => row[1], 'date' => Time.at(row[2]).to_s}
  end

  cgi = CGI.new
  cgi.out {
    output = "<style>body { background-color: lightgreen; color: blue; } .container { width: 80%; margin: auto; } .message { padding-top: 1em; padding-bottom: 1em; } .name { float:left; font-family:Copperplate; } .date { float: right; }</style><div class='container'>"
    results.each do |row|
      output << "<div class='message'>#{row['message']}</div><hr><div class='name'>#{row['name']}</div><div class='date'>#{row['date']}</div><hr style='clear:both;'>"
    end
    output << "<center><p>Sign the guestbook! <form action='guestbook_submit.rb' method='post'><input type='text' placeholder='name' name='name'><br><br><textarea name='message' rows=10 cols=30></textarea><br><br><input type='submit' value='Sign'></form></p><p><a href='../webring.html'>BACK to the homepage!!!</a></p></center>"
    output << "</div>"
    output
  }
ensure
  db.close if db
end
