<head>
  <link rel="stylesheet" href="../../bootstrap3/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../bootstrap3/css/bootstrap-theme.min.css">
  <link rel='stylesheet' href='../code2.css'>
  <script src='/js/ga.js' async></script>
</head>

<body>
  <div class='container'>
    <div class='row'>
      <div class='col-md-12'>
        <h1><a href='../index.html'>code][</a></h1>
      </div>
    </div>

    <div class='row'>
      <div class='col-md-offset-3 col-md-6 text-center'>
        <p>this is a work in progress. it is incomplete and contains errors. please send questions or improvements to: <a href="mailto:josh@extropicstudios.com">josh@extropicstudios.com</a></p>
      </div>
    </div>

    <hr>

    <?php
      include 'database.php';
      $mysqli = new mysqli(HOST, USERNAME, PASSWORD, DATABASE);
      if ($mysqli->connect_errno) {
        die('Can\'t connect to database.');
      }
      $filtered = false;
      if (intval($_GET['creator'] > 0)) {
        $query = 'SELECT * FROM creators WHERE creator_id=' . intval($_GET['creator']);
        $result = $mysqli->query($query);
        $row = $result->fetch_assoc();
        echo '<div class="row"><div class="col-md-12 text-center"><h2>Showing only software written by ' . $row['creator_name'] . '</h2></div></div><hr>';
        $filtered = true;
      }
      if (intval($_GET['publisher'] > 0)) {
        $query = 'SELECT * FROM publishers WHERE publisher_id=' . intval($_GET['publisher']);
        $result = $mysqli->query($query);
        $row = $result->fetch_assoc();
        echo '<div class="row"><div class="col-md-12 text-center"><h2>Showing only software published by ' . $row['publisher_name'] . '</h2></div></div><hr>';
        $filtered = true;
      }
      if (intval($_GET['year'] > 0)) {
        echo '<div class="row"><div class="col-md-12 text-center"><h2>Showing only software released in ' . $_GET['year'] . '</h2></div></div><hr>';
        $filtered = true;
      }
      if ($filtered) {
        echo '<div class="row"><div class="col-md-12 text-center"><a href="software.php">&nbsp;Show all software&nbsp;</a></div></div><hr>';
      }
    ?>

    <div class='row'>
      <div class='col-md-12 text-center'>
        <a href="#A">&nbsp;A&nbsp;</a>
        <a href="#B">&nbsp;B&nbsp;</a>
        <a href="#C">&nbsp;C&nbsp;</a>
        <a href="#D">&nbsp;D&nbsp;</a>
        <a href="#E">&nbsp;E&nbsp;</a>
        <a href="#F">&nbsp;F&nbsp;</a>
        <a href="#G">&nbsp;G&nbsp;</a>
        <a href="#H">&nbsp;H&nbsp;</a>
        <a href="#I">&nbsp;I&nbsp;</a>
        <a href="#J">&nbsp;J&nbsp;</a>
        <a href="#K">&nbsp;K&nbsp;</a>
        <a href="#L">&nbsp;L&nbsp;</a>
        <a href="#M">&nbsp;M&nbsp;</a>
        <a href="#N">&nbsp;N&nbsp;</a>
        <a href="#O">&nbsp;O&nbsp;</a>
        <a href="#P">&nbsp;P&nbsp;</a>
        <a href="#Q">&nbsp;Q&nbsp;</a>
        <a href="#R">&nbsp;R&nbsp;</a>
        <a href="#S">&nbsp;S&nbsp;</a>
        <a href="#T">&nbsp;T&nbsp;</a>
        <a href="#U">&nbsp;U&nbsp;</a>
        <a href="#V">&nbsp;V&nbsp;</a>
        <a href="#W">&nbsp;W&nbsp;</a>
        <a href="#X">&nbsp;X&nbsp;</a>
        <a href="#Y">&nbsp;Y&nbsp;</a>
        <a href="#Z">&nbsp;Z&nbsp;</a>
      </div>
    </div>

    <hr>

    <?php

      $query = 'SELECT * FROM software LEFT JOIN creators USING (creator_id) LEFT JOIN publishers USING (publisher_id) WHERE platform_id=1 AND is_game=0';

      if (intval($_GET['creator']) > 0) {
        $query .= ' AND creator_id=' . intval($_GET['creator']);
      }
      if (intval($_GET['publisher']) > 0) {
        $query .= ' AND publisher_id=' . intval($_GET['publisher']);
      }
      if (intval($_GET['year']) > 0) {
        $query .= ' AND published_date=\'' . intval($_GET['year']) . '-01-01\'';
      }

      $query .= ' ORDER BY software_name';

      $result = $mysqli->query($query);

      $current_letter = '';
    ?>

    <?php while ($row = $result->fetch_assoc()): ?>
      <?php $new_letter = strtoupper(substr($row['software_name'], 0, 1)); ?>
      <?php if ($new_letter != $current_letter): ?>
        <?php if (!empty($current_letter)): ?>
          <?php $first_letter = true; ?>
          </div></div>
        <?php endif; ?>
        <?php $current_letter = $new_letter; ?>
        <div class='row'>
          <div class='col-md-1 text-center'>
            <a name='<?=$current_letter?>'></a>
            <h1><?=$current_letter?></h1>
          </div>
          <div class='col-md-10' style='border: solid lime; border-width: <?php echo ($first_letter) ? '0 1em 1em' : '1em'; ?>;'>
            <div class='row'>
              <?php $first_letter = false; ?>
      <?php else: ?>
        <div class='row' style='border-top: 1em solid lime;'>
      <?php endif; ?>

        <div class='col-md-12'>
          <h3><?=$row['software_name']?></h3>
          <table class='table'>
            <?php if (!empty($row['creator_name'])): ?>
              <tr>
                <td>Creator:</td>
                <td><a href='software.php?creator=<?=$row['creator_id']?>'>&nbsp;<?=$row['creator_name']?>&nbsp;</a></td>
              </tr>
            <?php endif; ?>
            <?php if (!empty($row['publisher_name'])): ?>
              <tr>
                <td>Publisher:</td>
                <td><a href='software.php?publisher=<?=$row['publisher_id']?>'>&nbsp;<?=$row['publisher_name']?>&nbsp;</a></td>
              </tr>
            <?php endif; ?>
            <?php if (!empty($row['published_date'])): ?>
              <tr>
                <td>Published:</td>
                <td><a href='software.php?year=<?=date('Y', strtotime($row['published_date']))?>'>&nbsp;<?=date('Y', strtotime($row['published_date']))?>&nbsp;</td>
              </tr>
            <?php endif;?>
            <?php if (!empty($row['version'])): ?>
              <tr>
                <td>Latest Version:</td>
                <td><?=$row['version']?></td>
              </tr>
            <?php endif; ?>
            <?php if (!empty($row['requirements'])): ?>
              <tr>
                <td>Requirements:</td>
                <td><?=$row['requirements']?></td>
              </tr>
            <?php endif; ?>
            <?php if (!empty($row['filename'])): ?>
              <tr>
                <td colspan=2 class='text-center'>
                  <a href='https://s3.amazonaws.com/codexx/code2/software/<?=$row['filename']?>' style='line-height: 17px; font-size: 18px;'>
                    ************<br>
                    * Download *<br>
                    ************
                  </a>
                </td>
              </tr>
            <?php endif; ?>
          </table>
          <?php if (!empty($row['description'])): ?>
            <p><?=$row['description']?></p>
          <?php endif?>
        </div>
      </div>
    <?php endwhile;?>
    </div></div>
    <br>
    <br>
  </div>

  <div class='adsense'>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- code][ leaderboard ad -->
    <ins class="adsbygoogle"
         style="display:inline-block;width:728px;height:90px"
         data-ad-client="ca-pub-8149261173769150"
         data-ad-slot="2197438193">
    </ins>
    <script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
  </div>
</body>
