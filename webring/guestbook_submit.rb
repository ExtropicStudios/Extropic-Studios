#!/usr/bin/env ruby

require 'cgi'
require 'rubygems'
require 'sqlite3'

begin

  # open the sqlite db, creating it if it doesn't exist
  db = SQLite3::Database.open "db/guestbook.db"

  cgi = CGI.new

  # get all the guestbook crap.
  # TODO: might need to paginate this at some point in the future
  if cgi.has_key? 'message' and cgi.has_key? 'name' then
    db.execute "INSERT INTO guestbook (message, name, date) VALUES (?, ?, ?)", cgi.params['message'], cgi.params['name'], Time.now.to_i
  end

  print cgi.header('Status' => '301 REDIRECT', "Location" => "https://www.extropicstudios.com/webring/guestbook.rb")
ensure
  db.close if db
end
