$(document).ready(function() {

  $("#show-about").click(function() {
    $("#about").fadeIn('fast');
  });

  $("#hide-about").click(function() {
    $("#about").fadeOut('fast');
  });

  $(".textbox").focus();

  $("#ip-address").val(myIP());
  $("#useragent").val(navigator.userAgent);

  $("body").keydown(function(event) {
    /*if (event.which == 112) { // F1
      event.preventDefault();
      alert("Help!");
    } else*/ if (event.which == 113) { // F2
      event.preventDefault();
      window.location.href = "1.html";
    } else if (event.which == 114) { // F3
      event.preventDefault();
      window.location.href = "last.html";
    } else if (event.which == 115) { // F4
      event.preventDefault();
      window.location.href = "https://www.extropicstudios.com";
    }
  });
});

function myIP() {
    if (window.XMLHttpRequest) xmlhttp = new XMLHttpRequest();
    else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

    xmlhttp.open("GET","http://api.hostip.info/get_html.php",false);
    xmlhttp.send();

    hostipInfo = xmlhttp.responseText.split("\n");

    for (i=0; hostipInfo.length >= i; i++) {
        ipAddress = hostipInfo[i].split(":");
        if ( ipAddress[0] == "IP" ) return ipAddress[1];
    }

    return false;
}
